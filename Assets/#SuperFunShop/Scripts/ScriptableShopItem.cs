using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/NewShopItem", order = 1)]
public class ScriptableShopItem : ScriptableObject
{
    [FormerlySerializedAs("Name")] [SerializeField]
    private string _name;
    [FormerlySerializedAs("Color")] [SerializeField]
    private Material _color;
    
    public bool IsSame(ScriptableShopItem incomingItem)
    {
        if (_name == incomingItem.getName())
        {
            return true;
        }
        return false;
    }

    public Material GetColor()
    {
        return _color;
    }
    
    public void SetColor(Material newMaterial)
    {
        _color = newMaterial;
    }

    public void setName(string newName)
    {
        _name = newName;
    }
    
    public string getName()
    {
        return _name;
    }
}
