using System.Collections.Generic;
using UnityEngine;

namespace _SuperFunShop.Scripts.Player
{
    public class Inventory : MonoBehaviour
    {
        public List<ScriptableShopItem> itemsInInventory = new List<ScriptableShopItem>();

        public void Add(ScriptableShopItem ScriptableShopItem)
        {
            if (itemsInInventory.Count >= 4) return;
            itemsInInventory.Add(ScriptableShopItem);
        }
    }
}
