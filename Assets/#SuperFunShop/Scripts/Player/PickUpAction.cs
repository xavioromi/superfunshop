using System.Collections;
using System.Collections.Generic;
using _SuperFunShop.Scripts.Player;
using Unity.VisualScripting;
using UnityEngine;

public class PickUpAction : MonoBehaviour
{
    public KeyCode pickKey = KeyCode.Space;
    public Inventory inventory;
    public ScriptableShopItem itemDetected;
    private GameObject ItemModel;
    public PlayerController myPlayerController;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(pickKey))
        {
            PickItem();
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.tag == "Item")
        {
            ItemModel = c.gameObject;
            itemDetected = ItemModel.GetComponent<ShopItem>().MyScriptableShopItem;
        }
        
    }
    
    void OnTriggerExit(Collider c)
    {
        if (c.tag == "Item")
        {
            itemDetected = null;
        }
        
    }

    void PickItem()
    {
        if (itemDetected && myPlayerController.myInventory.itemsInInventory.Count <= 4)
        {
            inventory.Add(itemDetected);
            itemDetected = null;
            ItemModel.SetActive(false);
            myPlayerController.UpdateVisualClues();
        }
    }
}
