using System;
using System.Collections;
using System.Collections.Generic;
using _SuperFunShop.Scripts.Player;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Inventory myInventory;
    [SerializeField]
    public List<GameObject> playerItemsVisualClue;
    public Material whiteMaterial;

    public int myScore = 0;

    private void Start()
    {
        UpdateVisualClues();
    }

    public void GiveItemsToClient(ClientRequiredItem clientReqItem)
    {
        for (int pi = 0; pi < myInventory.itemsInInventory.Count; pi++)
        {
            if (myInventory.itemsInInventory[pi].IsSame(clientReqItem.RequiredItem))
            {
                IncreaseScore(1);
                clientReqItem.itemIsDelivered = true;
                myInventory.itemsInInventory.RemoveAt(pi);
                return;
            }
        }
    }

    public void IncreaseScore(int value)
    {
        myScore += value;
        Debug.Log("Player: " + this.gameObject.name + " Score is: " + myScore);
    }

    public void UpdateVisualClues()
    {
        for (int i = 0; i < playerItemsVisualClue.Count; i++)
        {
            playerItemsVisualClue[i].SetActive(false);
            playerItemsVisualClue[i].GetComponent<MeshRenderer>().material = whiteMaterial;
        }

        for (int imm = 0; imm < myInventory.itemsInInventory.Count; imm++)
        {
            playerItemsVisualClue[imm].GetComponent<MeshRenderer>().material = myInventory.itemsInInventory[imm].GetColor();
            playerItemsVisualClue[imm].SetActive(true);
        }
    }
}
