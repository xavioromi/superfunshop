using UnityEngine;

public class GameController : MonoBehaviour
{
    //Item Spawn Rate
    public static int ItemSpawnRateMin = 3;
    public static int ItemSpawnRateMax = 10;
    public static int InitialAvailableItems = 5;

    //Client Spawn Rate
    public static int ClientSpawnRateMin = 5;
    public static int ClientSpawnRateMax = 10;

    //Item
    
    //Timer
    public static float TimerInSeconds = 40;
    public static bool _timerIsRunning;
    public static bool _timerFinished;
    private NewTimer _timer;
    private const KeyCode _startTimerKey = KeyCode.M;

    public void Start()
    {
        PauseGame();
        _timer = gameObject.AddComponent<NewTimer>();
        _timer.SetDuration(TimerInSeconds);
        _timer.OnTimerFinished += PauseGame;
    }
    
    public void Update()
    {
        if (Input.GetKeyDown(_startTimerKey) && _timerIsRunning == false )
        {
            StartGame();
        }
    }
    
    void PauseGame()
    {
        _timerFinished = false;
        Time.timeScale = 0;
    }
    void StartGame()
    {
        _timerIsRunning = true;
        _timer.StartTimer();
        Time.timeScale = 1;
    }

}
