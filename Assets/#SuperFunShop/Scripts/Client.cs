using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class Client : MonoBehaviour
{
    public GameObject ClientModel;
    public List<GameObject> clientItemsVisualClue;

    private PlayerController PlayerInCollider;

    [ShowInInspector]
    public List<ClientRequiredItem> ClientShopItemScriptableList = new List<ClientRequiredItem>();

    public List<ScriptableShopItem> availableItems = new List<ScriptableShopItem>();

    private void Start()
    {
        randomItem();
        HideVisualClues();
        UpdateVisualClues();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            PlayerInCollider = collider.gameObject.GetComponent<PlayerController>();

            for (int i = 0; i < ClientShopItemScriptableList.Count; i++)
            {
                if (!ClientShopItemScriptableList[i].itemIsDelivered)
                {
                    PlayerInCollider.GiveItemsToClient(ClientShopItemScriptableList[i]);
                }

            }

            CheckForAllItemsDelivered();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerInCollider = null;
    }

    private void CheckForAllItemsDelivered()
    {
        int itemsDelivered = 0;
        for (int i = 0; i < ClientShopItemScriptableList.Count; i++)
        {
            if (ClientShopItemScriptableList[i].itemIsDelivered)
            {
                itemsDelivered += 1;
            }

        }

        if(itemsDelivered == ClientShopItemScriptableList.Count)
        {
            ClientLeave();
            PlayerInCollider.IncreaseScore(2);
            Debug.Log("Client is HAPPY!");
        }

        UpdateVisualClues();
        PlayerInCollider.UpdateVisualClues();
    }

    private void ClientLeave()
    {
        ClientModel.SetActive(false);
        HideVisualClues();
        StartCoroutine(InitializateClientRandomTimer());
    }

    private void randomItem()
    {
        ClientShopItemScriptableList = new List<ClientRequiredItem>(3);

        for (int i = 0; i < 3; i++)
        {
            int temp = Random.Range(0, availableItems.Count);
            ClientShopItemScriptableList.Add(new ClientRequiredItem());
            ClientShopItemScriptableList[i].RequiredItem = availableItems[temp];
            ClientShopItemScriptableList[i].itemIsDelivered = false;
        }

    }

    private void ClientEnter()
    {
        ClientModel.SetActive(true);
        randomItem();
        UpdateVisualClues();
    }

    private void UpdateVisualClues()
    {
        for (int i = 0; i < clientItemsVisualClue.Count; i++)
        {
            if (ClientShopItemScriptableList[i] == null) return;
            
            if (!ClientShopItemScriptableList[i].itemIsDelivered)
            {
                clientItemsVisualClue[i].SetActive(true);
                clientItemsVisualClue[i].GetComponent<MeshRenderer>().material = ClientShopItemScriptableList[i].RequiredItem.GetColor();
            }
            else
            {
                clientItemsVisualClue[i].SetActive(false);
            }
        }
    }

    private void HideVisualClues()
    {
        for (int i = 0; i < clientItemsVisualClue.Count; i++)
        {
            clientItemsVisualClue[i].SetActive(false);
        }
    }
    
    IEnumerator InitializateClientRandomTimer()
    {
        int wait_time = Random.Range (GameController.ClientSpawnRateMin, GameController.ClientSpawnRateMax);
        yield return new WaitForSeconds (wait_time);
        ClientEnter();
        //InitializateItem(stageItems[Random.Range (0, stageItems.Count)]);
    }
}

public class ClientRequiredItem
{
    public ScriptableShopItem RequiredItem;
    public bool itemIsDelivered;
}
