using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class NewTimer : MonoBehaviour
{
    private float _duration = 10;
    public event Action OnTimerFinished;

    public void SetDuration(float newDuration)
    {
        _duration = newDuration;
    }

    public void StartTimer()
    {
        StartCoroutine(TimerCoroutine());
    }

    private IEnumerator TimerCoroutine()
    {
        yield return new WaitForSeconds(_duration);
        if (OnTimerFinished != null)
        {
            OnTimerFinished();
        }
    }
    
    public string DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;
        float minutes = Mathf.FloorToInt(timeToDisplay / 60); 
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
