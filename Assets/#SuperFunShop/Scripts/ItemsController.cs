using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsController : MonoBehaviour
{
    public List<GameObject> stageGameObjectItems = new List<GameObject>();
    public List<ScriptableShopItem> listOfAvailableScriptableItems = new List<ScriptableShopItem>();

    void Start()
    {
        GameStartInitializeItems();
    }

    private void GameStartInitializeItems()
    {

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
            stageGameObjectItems.Add(child.gameObject);
        }

        //Initialize a N ammount of shop items at the beggining of the game
        for (int i = 0; i < GameController.InitialAvailableItems; i++)
        {
            int current = Random.Range(0, stageGameObjectItems.Count);

            if (stageGameObjectItems[current].activeInHierarchy == false)
            {
                InitializateItem(stageGameObjectItems[current],false);
            }
            else
            {
                //Repeat process
                i -= 1;
            }
        }
        StartCoroutine(InitializateItemRandomTimer());
    }

    private void InitializateItem(GameObject myObject, bool activateOther)
    {
        if (myObject.gameObject.activeInHierarchy)
        {
            if (activateOther) StartCoroutine(InitializateItemRandomTimer());
            return;
        }

        myObject.SetActive(true);
        

        int temp = Random.Range(0, listOfAvailableScriptableItems.Count);
        myObject.GetComponent<ShopItem>().MyScriptableShopItem = listOfAvailableScriptableItems[temp];
        myObject.GetComponent<ShopItem>().UpdateColor();

        if (activateOther) StartCoroutine(InitializateItemRandomTimer()) ;
    }

    IEnumerator InitializateItemRandomTimer()
    {
        int wait_time = Random.Range (GameController.ItemSpawnRateMin, GameController.ClientSpawnRateMax);
        yield return new WaitForSeconds (wait_time);
        int temp = Random.Range(0, stageGameObjectItems.Count);
        InitializateItem(stageGameObjectItems[temp],true);
    }
}
