using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ShopItem : MonoBehaviour
{
    [SerializeField]
    public ScriptableShopItem MyScriptableShopItem;
    
    void Start()
    {
        UpdateColor();
    }

    public void UpdateColor()
    {
        gameObject.GetComponent<Renderer>().material = MyScriptableShopItem.GetColor();
    }

    public void setScriptableShopItem(ScriptableShopItem newScriptableShopItem)
    {
        MyScriptableShopItem = newScriptableShopItem;
    }
}
