using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class NewTimerTest
{
    private bool _eventDispatched;
    private NewTimer _timer;

    [SetUp]
    public void SetUp()
    {
        GameObject gameObject = new GameObject();
        NewTimer timer = gameObject.AddComponent<NewTimer>();
        _timer = timer;
        _timer.OnTimerFinished += EventDispached;
    }

    [UnityTest]
    public IEnumerator given_a_timer__should_dispatch_event_at_finish()
    {
        
        _timer.SetDuration(2);
        
        _timer.StartTimer();
        
        while (!_eventDispatched)
        {
            yield return null;
        }

        Assert.IsTrue(_eventDispatched);
    }
    
    private void EventDispached()
    {
        _eventDispatched = true;
    }
}
