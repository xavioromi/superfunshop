using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

public class ShopItemTest
{
    private ShopItem _sut;

    [SetUp]
    public void SetUp()
    {
        var gameObject = new GameObject();
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        _sut = gameObject.AddComponent<ShopItem>();
    }
    
    [Test]
    public void given_a_scriptable_shop_item_with_material__should_update_material()
    {
        var sShopItem = ScriptableObject.CreateInstance<ScriptableShopItem>();
        var material = Resources.Load<Material>("Assets/#SuperFunShop/Materials/Blue.mat");
        sShopItem.SetColor(material);
        _sut.setScriptableShopItem(sShopItem);
        
        _sut.UpdateColor();
        
        Assert.AreSame(material, _sut.GetComponent<Renderer>().sharedMaterial);
    }

}
