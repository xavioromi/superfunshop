using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TimerTest
{
    private bool _eventDispatched;
    private NewTimer _timer;

    [SetUp]
    public void SetUp()
    {
        GameObject gameObject = new GameObject();
        NewTimer timer = gameObject.AddComponent<NewTimer>();
        _timer = timer;
    }

    [Test]
    public void given_a_seconds_in_float__should_return_string_with_minutes_and_seconds()
    {
        var response = _timer.DisplayTime(100);

        Assert.AreEqual("01:41",response);
    }
}
