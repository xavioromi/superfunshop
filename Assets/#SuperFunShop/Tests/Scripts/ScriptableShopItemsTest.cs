using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class ScriptableShopItemsTest
{
    private ScriptableShopItem _sut;

    [SetUp]
    public void SetUp()
    {
        _sut = ScriptableObject.CreateInstance<ScriptableShopItem>();
    }
    
    [Test]
    public void given_a_implementation_without_same_name_when_compare_should_return_false()
    {
        _sut.setName("Any name");
        var otherScriptableShopItem = ScriptableObject.CreateInstance<ScriptableShopItem>();
        otherScriptableShopItem.setName("Other name");
        Assert.False(_sut.IsSame(otherScriptableShopItem));
    }
    
    [Test]
    public void given_a_implementation_with_same_name_when_compare_should_return_true()
    {
        var sameName = "Same Name";
        _sut.setName(sameName);
        var otherScriptableShopItem = ScriptableObject.CreateInstance<ScriptableShopItem>();
        otherScriptableShopItem.setName(sameName);
        Assert.True(_sut.IsSame(otherScriptableShopItem));
    }
}
